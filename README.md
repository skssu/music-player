# Music Player
[![Gitlab Status](https://gitlab.com/exitlive/music-player/badges/master/build.svg)](https://gitlab.com/exitlive/music-player/pipelines)
[![Pub Status](https://img.shields.io/pub/v/music_player.svg)](https://pub.dartlang.org/packages/music_player)

A music player flutter plugin that uses the native api to
show the currently playing track with cover image and controls.

![Example Notification](example/notification.png)

## WARNING

**This plugin is still in development**

## Prerequisites

**Android**:

- [Android 5.0 Lollipop (API 21) or higher][versionDashboard]
- Kotlin support
- AndroidX compatibility

[versionDashboard]: https://developer.android.com/about/dashboards/

**iOS**:

- Swift support
- iOS 10.* or higher, because of some newly introduced audio playback features


## Usage

A [sample project](example/README.md) can be found in the `example` folder.


## Additional installation steps

### Android

- Set the `minSdkVersion` in `android/app/build.gradle` to 21
- [Migrate to AndroidX][migrateToAndroidX]
- Add Kotlin support (In AndroidStudio "Refactor > Migrate to AndroidX..")
- Add the `FOREGROUND_SERVICE` permission and the `MusicPlayerService` to `android/app/src/main/AndroidManifest.xml` (see below)

```xml
<manifest xmlns:android="..." package="...">

    <!-- Add following permission -->
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />

    <application> 
        ...
        <activity>...</activity>
        
        <!-- Add the following service -->
        <service
            android:name="live.exit.musicplayer.MusicPlayerService"
            android:enabled="true" />
    </application>
</manifest>

```

[migrateToAndroidX]: https://flutter.dev/docs/development/packages-and-plugins/androidx-compatibility

### iOS

TODO

